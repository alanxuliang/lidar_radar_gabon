# Lidar and Radar relationships over Gabon

## Objectives from Sassan:

1. Use all the ALOS data over the four years over Gabon at 1-ha resolution
2. Use all the lidar we have acquired over Gabon
3. Use our model and estimation biomass from lidar at 1-ha
4. Overlay the radar over lidar for pixel to pixel comparison and extract mean canopy height and biomass from lidar and radar and develop models and saturation levels. Do this for all four radar imagery so we can reduce the noise. 
5. In analyzing the radar and lidar, we can also do a scaling problem, using 100 m, 250 m, 500 m, and 1000m scales for height, biomass, and radar comparison. This will help to create a much better relation.
6. Use the model developed to estimate biomass and height within the saturation range < 100-150 Mg/ha) of radar for the entire Gabon.

## References:

Git project URL: 
git@gitlab.com:alanxuliang/lidar_radar_gabon.git

Sourcetree app:
https://www.sourcetreeapp.com/


## Specific tasks:

* Resample all layers to be the same resolution and spatial extent using spatial average (LC map using majority)
* Extract valid pixels using LC maps (LC == 2 or 5) and rearrange them to table format (e.g. output to csv)
* Try different resolutions: 100 m, 250 m, 500 m, and 1000 m


## Jobs done:

